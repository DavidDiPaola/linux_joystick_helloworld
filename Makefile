# 2018 David DiPaola
# licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

BIN = linux_joystick_helloworld

_CFLAGS = -Wall -Wextra \
	$(CFLAGS)

.PHONY: all
all: $(BIN)

$(BIN): main.c
	$(CC) $(_CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BIN)

