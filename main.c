/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>

#include <errno.h>

#include <linux/joystick.h>

int
main() {
	printf("open() ..." "\n");
	int  js_fd = -1;
	char js_path[64];
	#define js_path_BASE "/dev/input/js"
	for (int i=0; (i<=9 && js_fd<0); i++) {
		snprintf(js_path, sizeof(js_path), js_path_BASE "%i", i);
		js_fd = open(js_path, O_RDONLY | O_NONBLOCK);
	}
	if (js_fd < 0) {
		fprintf(stderr, "ERROR: could not open any joystick devices at " js_path_BASE "X" "\n");
		return 1;
	}
	printf("open() success (%s)" "\n", js_path);
	#undef js_path_BASE

	printf("ioctl() %s :" "\n", js_path);
	char c;
	ioctl(js_fd, JSIOCGAXES,    &c);         printf("\t" "number of axes: %i" "\n", c);
	ioctl(js_fd, JSIOCGBUTTONS, &c);         printf("\t" "number of buttons: %i" "\n", c);
	char s[128] = "Unknown";
	ioctl(js_fd, JSIOCGNAME(sizeof(s)), s);  printf("\t" "name: %s" "\n", s);

	printf("press HOME button to exit" "\n");
	int done = 0;
	while (!done) {
		usleep(16666);

		struct js_event event;
		ssize_t amount = read(js_fd, &event, sizeof(event));
		if (amount < (ssize_t)sizeof(event)) {
			if (errno != EAGAIN) {
				fprintf(stderr, "%s: read() expected %zi bytes but got %zi bytes" "\n", js_path, amount, sizeof(event));
				return 1;
			}
			else {
				continue;
			}
		}

		if (event.type & JS_EVENT_AXIS) {
			printf("axis %i = %i" "\n", event.number, event.value);
		}
		else if (event.type & JS_EVENT_BUTTON) {
			printf("button %i = %i" "\n", event.number, event.value);
			if ((event.number == 8) && event.value) {
				done = 1;
			}
		}
		else if (event.type & JS_EVENT_INIT) {
			printf("init" "\n");
		}
		else {
			printf("WARNING: event type unknown (0x%02X)" "\n", event.type);
		}
	}

	close(js_fd);
}

